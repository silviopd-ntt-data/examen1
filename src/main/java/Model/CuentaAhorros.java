package Model;

public class CuentaAhorros extends CuentaBancaria {
    private double interesMensual;
    private int cantidadMeses;

    public CuentaAhorros(String numeroCuenta, double montoApertura, Cliente cliente, double interesMensual, int cantidadMeses) {
        super(numeroCuenta, montoApertura, cliente);
        this.interesMensual = interesMensual;
        this.cantidadMeses = cantidadMeses;
    }

    public double getInteresMensual() {
        return interesMensual;
    }

    public void setInteresMensual(double interesMensual) {
        this.interesMensual = interesMensual;
    }

    public int getCantidadMeses() {
        return cantidadMeses;
    }

    public void setCantidadMeses(int cantidadMeses) {
        this.cantidadMeses = cantidadMeses;
    }

    public double calcularSaldo(){
        return getMontoApertura()+(getCantidadMeses()*getInteresMensual());
    }

    @Override
    public String toString() {
        return "CuentaAhorros{" +
                "cliente=" + getCliente() +
                ", montoApertura=" + getMontoApertura() +
                ", interesMensual=" + interesMensual +
                ", cantidadMeses=" + cantidadMeses +
                ", saldo=" + calcularSaldo() +
                '}';
    }
}
